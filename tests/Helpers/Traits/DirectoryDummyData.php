<?php declare(strict_types=1);

namespace Tests\Helpers\Traits;

/**
 * Provides data for tests (also in PullService module)
 *
 * Trait DirectoryDummyData
 *
 * @package Tests\Pullservice\Helpers\Traits
 */
trait DirectoryDummyData
{
    public function countryDummyData(): array
    {
        return [
            [
                'id'      => 1,
                'name'    => 'Russia',
                'alpha2'  => 'ru',
                'alpha3'  => 'rus',
                'fiat_id' => 1,
            ],
        ];
    }

    public function cryptoDummyData(): array
    {
        return [
            [
                'id'   => 1,
                'name' => 'Bitcoin',
                'code' => 'btc',
            ],
        ];
    }

    public function fiatDummyData(): array
    {
        return [
            [
                'id'   => 1,
                'name' => 'Ruble',
                'code' => 'rub',
            ],
        ];
    }

    public function paymentDummyData(): array
    {
        return [
            [
                'id'   => 1,
                'name' => 'Qiwi',
                'code' => 'qiwi',
            ],
        ];
    }

    public function marketDummyData(): array
    {
        return [
            [
                'id'   => 1,
                'name' => 'Risex',
                'url'  => 'risex.net',
            ],
        ];
    }

    public function bankDummyData(): array
    {
        return [
            [
                'id'   => 1,
                'name' => 'Bank1',
                'code' => 'bb1',
            ],
            [
                'id'   => 2,
                'name' => 'Bank2',
                'code' => 'bb2',
            ],
            [
                'id'   => 3,
                'name' => 'Bank3',
                'code' => 'bb3',
            ],
        ];
    }
}
