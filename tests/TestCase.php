<?php declare(strict_types=1);

namespace Tests;

use Orchestra\Testbench\TestCase as BaseTestCase;
use p2pwatch\DirectoryDataService\DirectoryDataServiceProvider;

/**
 * Class TestCase
 *
 * @package       Tests
 * @backupGlobals disabled
 */
abstract class TestCase extends BaseTestCase
{
    public const CACHE_PATH = 'p2pw-directorydataservice.directory_cache_key';

    protected function getPackageProviders($app): array
    {
        return [DirectoryDataServiceProvider::class];
    }
}
