<?php declare(strict_types=1);

namespace Tests\Backend\Unit\Repositories;

use Illuminate\Support\Facades\Cache;
use p2pwatch\DirectoryDataService\Contracts\Repositories\BankCachedRepoContract;
use p2pwatch\DirectoryDataService\Contracts\Repositories\CachedRepoContract;
use p2pwatch\DirectoryDataService\Contracts\Repositories\CountryCachedRepoContract;
use p2pwatch\DirectoryDataService\Contracts\Repositories\CryptoCachedRepoContract;
use p2pwatch\DirectoryDataService\Contracts\Repositories\FiatCachedRepoContract;
use p2pwatch\DirectoryDataService\Contracts\Repositories\MarketCachedRepoContract;
use p2pwatch\DirectoryDataService\Contracts\Repositories\PaymentCachedRepoContract;
use p2pwatch\DirectoryDataService\Repositories\BankCachedRepo;
use p2pwatch\DirectoryDataService\Repositories\CountryCachedRepo;
use p2pwatch\DirectoryDataService\Repositories\CryptoCachedRepo;
use p2pwatch\DirectoryDataService\Repositories\FiatCachedRepo;
use p2pwatch\DirectoryDataService\Repositories\MarketCachedRepo;
use p2pwatch\DirectoryDataService\Repositories\PaymentCachedRepo;
use Tests\Helpers\Traits\DirectoryDummyData;
use Tests\TestCase;

class AbstractCachedRepoTest extends TestCase
{
    use DirectoryDummyData;

    /**
     * @return array
     */
    public function instanceTestDataProvider(): array
    {
        return [
            [CountryCachedRepoContract::class, CountryCachedRepo::class],
            [CryptoCachedRepoContract::class, CryptoCachedRepo::class],
            [FiatCachedRepoContract::class, FiatCachedRepo::class],
            [PaymentCachedRepoContract::class, PaymentCachedRepo::class],
            [MarketCachedRepoContract::class, MarketCachedRepo::class],
            [BankCachedRepoContract::class, BankCachedRepo::class],
        ];
    }

    /**
     * @dataProvider instanceTestDataProvider
     *
     * @param $contract
     * @param $class
     */
    public function testInstantiate($contract, $class): void
    {
        /** @var CachedRepoContract $repo */
        $repo = app($contract);

        $this->assertInstanceOf($contract, $repo);
        $this->assertInstanceOf($class, $repo);

        /** @var CachedRepoContract $repo */
        $repo = app($class);

        $this->assertInstanceOf($contract, $repo);
        $this->assertInstanceOf($class, $repo);
    }

    /**
     * @return array
     */
    public function dataStructureProvider(): array
    {
        return [
            [
                CountryCachedRepo::class,
                'country',
                $this->countryDummyData(),
            ],
            [
                CryptoCachedRepo::class,
                'crypto',
                $this->cryptoDummyData(),
            ],
            [
                FiatCachedRepo::class,
                'fiat',
                $this->fiatDummyData(),
            ],
            [
                PaymentCachedRepo::class,
                'payment',
                $this->paymentDummyData(),
            ],
            [
                MarketCachedRepo::class,
                'market',
                $this->marketDummyData(),
            ],
            [
                BankCachedRepo::class,
                'bank',
                $this->bankDummyData(),
            ],
        ];
    }

    /**
     * @dataProvider dataStructureProvider
     *
     * @param $class
     * @param $cacheKey
     * @param $testData
     */
    public function testCorrectData($class, $cacheKey, $testData): void
    {
        Cache::forever(config(static::CACHE_PATH) . $cacheKey, $testData);

        /** @var CachedRepoContract $repo */
        $repo = app($class);
        $result = $repo->all();

        // assert
        $this->assertEquals($testData, $result);
    }
}
