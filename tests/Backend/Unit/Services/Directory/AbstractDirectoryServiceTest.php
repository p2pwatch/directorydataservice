<?php declare(strict_types=1);

namespace Tests\Backend\Unit\Services\Directory;

use Illuminate\Support\Facades\Cache;
use p2pwatch\DirectoryDataService\Contracts\Services\BankDirectoryServiceContract;
use p2pwatch\DirectoryDataService\Contracts\Services\CountryDirectoryServiceContract;
use p2pwatch\DirectoryDataService\Contracts\Services\CryptoDirectoryServiceContract;
use p2pwatch\DirectoryDataService\Contracts\Services\DirectoryServiceContract;
use p2pwatch\DirectoryDataService\Contracts\Services\FiatDirectoryServiceContract;
use p2pwatch\DirectoryDataService\Contracts\Services\MarketDirectoryServiceContract;
use p2pwatch\DirectoryDataService\Contracts\Services\PaymentDirectoryServiceContract;
use p2pwatch\DirectoryDataService\Services\BankDirectoryService;
use p2pwatch\DirectoryDataService\Services\CountryDirectoryService;
use p2pwatch\DirectoryDataService\Services\CryptoDirectoryService;
use p2pwatch\DirectoryDataService\Services\FiatDirectoryService;
use p2pwatch\DirectoryDataService\Services\MarketDirectoryService;
use p2pwatch\DirectoryDataService\Services\PaymentDirectoryService;
use Tests\Helpers\Traits\DirectoryDummyData;
use Tests\TestCase;

class AbstractDirectoryServiceTest extends TestCase
{
    use DirectoryDummyData;

    /**
     * @return array
     */
    public function instanceTestDataProvider(): array
    {
        return [
            [CountryDirectoryServiceContract::class, CountryDirectoryService::class],
            [CryptoDirectoryServiceContract::class, CryptoDirectoryService::class],
            [FiatDirectoryServiceContract::class, FiatDirectoryService::class],
            [PaymentDirectoryServiceContract::class, PaymentDirectoryService::class],
            [MarketDirectoryServiceContract::class, MarketDirectoryService::class],
            [BankDirectoryServiceContract::class, BankDirectoryService::class],
        ];
    }

    /**
     * @dataProvider instanceTestDataProvider
     *
     * @param $contract
     * @param $class
     */
    public function testInstantiate($contract, $class): void
    {
        /** @var DirectoryServiceContract $service */
        $service = app($contract);

        $this->assertInstanceOf($contract, $service);
        $this->assertInstanceOf($class, $service);

        /** @var DirectoryServiceContract $service */
        $service = app($class);

        $this->assertInstanceOf($contract, $service);
        $this->assertInstanceOf($class, $service);
    }

    /**
     * @return array
     */
    public function dataStructureProvider(): array
    {
        return [
            [
                CountryDirectoryService::class,
                'country',
                $this->countryDummyData(),
            ],
            [
                CryptoDirectoryService::class,
                'crypto',
                $this->cryptoDummyData(),
            ],
            [
                FiatDirectoryService::class,
                'fiat',
                $this->fiatDummyData(),
            ],
            [
                PaymentDirectoryService::class,
                'payment',
                $this->paymentDummyData(),
            ],
            [
                MarketDirectoryService::class,
                'market',
                $this->marketDummyData(),
            ],
            [
                BankDirectoryService::class,
                'bank',
                $this->bankDummyData(),
            ],
        ];
    }

    /**
     * @dataProvider dataStructureProvider
     *
     * @param $class
     * @param $cacheKey
     * @param $testData
     */
    public function testDataStructure($class, $cacheKey, $testData): void
    {
        Cache::forever(config(static::CACHE_PATH) . $cacheKey, $testData);

        /** @var DirectoryServiceContract $service */
        $service = app($class);
        $result = $service->all();

        // assert
        $this->assertEquals($testData, $result);
    }
}
