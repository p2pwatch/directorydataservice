<?php declare(strict_types=1);

namespace Tests\Backend\Unit\Jobs;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Cache;
use p2pwatch\DirectoryDataService\Jobs\UpdateDirectoryData;
use Tests\Helpers\Traits\DirectoryDummyData;
use Tests\TestCase;

class UpdateDirectoryDataTest extends TestCase
{
    use DirectoryDummyData;

    protected function mockGuzzle(): void
    {
        // mock guzzle
        $this->app->bind(
            Client::class,
            function () {
                $mock = new MockHandler(
                    [
                        new Response(
                            200, [], json_encode(
                                   [
                                       'country' => $this->countryDummyData(),
                                       'crypto'  => $this->cryptoDummyData(),
                                       'fiat'    => $this->fiatDummyData(),
                                       'payment' => $this->paymentDummyData(),
                                       'market'  => $this->marketDummyData(),
                                       'bank'    => $this->bankDummyData(),
                                   ],
                                   JSON_THROW_ON_ERROR,
                                   512
                               )
                        ),
                    ]
                );

                $handler = HandlerStack::create($mock);

                return new Client(['handler' => $handler]);
            }
        );
    }

    public function testInstantiate(): void
    {
        /** @var UpdateDirectoryData $job */
        $job = app(UpdateDirectoryData::class);

        $this->assertInstanceOf(UpdateDirectoryData::class, $job);
    }

    /**
     * @throws GuzzleException
     */
    public function testFillsCacheDirectoryData(): void
    {
        config()->set('p2pw-directorydataservice.directory_token', 'token123');
        config()->set('p2pw-directorydataservice.directory_url', 'http://directory_url');
        $this->mockGuzzle();

        /** @var UpdateDirectoryData $job */
        $job = app(UpdateDirectoryData::class);
        $job->handle();

        // assert
        $this->assertCacheStored();
    }

    /**
     * Helper assert method
     */
    protected function assertCacheStored(): void
    {
        $this->assertEquals($this->countryDummyData(), Cache::get(config(static::CACHE_PATH) . 'country'));
        $this->assertEquals($this->cryptoDummyData(), Cache::get(config(static::CACHE_PATH) . 'crypto'));
        $this->assertEquals($this->fiatDummyData(), Cache::get(config(static::CACHE_PATH) . 'fiat'));
        $this->assertEquals($this->paymentDummyData(), Cache::get(config(static::CACHE_PATH) . 'payment'));
        $this->assertEquals($this->marketDummyData(), Cache::get(config(static::CACHE_PATH) . 'market'));
        $this->assertEquals($this->bankDummyData(), Cache::get(config(static::CACHE_PATH) . 'bank'));
    }

    /**
     * @throws GuzzleException
     */
    public function testUpdatesCacheDirectoryData(): void
    {
        config()->set('p2pw-directorydataservice.directory_token', '123');
        config()->set('p2pw-directorydataservice.directory_url', 'http://directory_url');
        $this->mockGuzzle();

        // setup
        Cache::forever(config(static::CACHE_PATH) . 'country', ['test']);
        Cache::forever(config(static::CACHE_PATH) . 'crypto', ['test']);
        Cache::forever(config(static::CACHE_PATH) . 'fiat', ['test']);
        Cache::forever(config(static::CACHE_PATH) . 'payment', ['test']);
        Cache::forever(config(static::CACHE_PATH) . 'market', ['test']);
        Cache::forever(config(static::CACHE_PATH) . 'bank', ['test']);

        /** @var UpdateDirectoryData $job */
        $job = app(UpdateDirectoryData::class);
        $job->handle();

        // assert
        $this->assertCacheStored();
    }
}
