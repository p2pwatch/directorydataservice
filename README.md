# DirectoryDataService

updates directory data on cron and stores in cache, provides services for retrieving saved data

## Install

add your ssh key to this repo

add to composer.json:
```
"repositories":[
    {
      "type": "vcs",
      "url": "git@bitbucket.org:p2pwatch/directorydataservice.git"
    }
],
```

`composer require p2pwatch/directorydataservice`

`php artisan vendor:publish`

and select this package's publish option

setup .env according to config file: p2pw-directorydataservice.php

```DIRECTORY_URL``` api endpoint with directory data (i.e. http://p2pwatch.io:5577/directory/all)

```DIRECTORY_TOKEN``` token to access directory data 


add in app/Console/Kernel.php:
```
use p2pwatch\DirectoryDataService\Jobs\UpdateDirectoryData;
...
protected function schedule(Schedule $schedule)
{
    // your other schedule
    ...
    
    $schedule->job(UpdateDirectoryData::class)->everyFiveMinutes();
}
```

setup laravel's CACHE_DRIVER (database, redis, file or array should be ok)

## Example usage with app()

instantiate services with app() container or thru DI: \
CountryDirectoryServiceContract \
CryptoDirectoryServiceContract \
FiatDirectoryServiceContract \
PaymentDirectoryServiceContract \
MarketDirectoryServiceContract \
BankDirectoryServiceContract

```
/** @var CountryDirectoryServiceContract $service **/
$service = app(CountryDirectoryServiceContract::class);
$service->...(see available methods)

```

## Example controller and DI

```
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use p2pwatch\DirectoryDataService\Contracts\Services\CountryDirectoryServiceContract;
use p2pwatch\DirectoryDataService\Contracts\Services\CryptoDirectoryServiceContract;
use p2pwatch\DirectoryDataService\Contracts\Services\FiatDirectoryServiceContract;
use p2pwatch\DirectoryDataService\Contracts\Services\MarketDirectoryServiceContract;
use p2pwatch\DirectoryDataService\Contracts\Services\PaymentDirectoryServiceContract;
use p2pwatch\DirectoryDataService\Contracts\Services\ExcludeUrlDirectoryServiceContract;

class DirectoryController extends Controller
{
    /**
     * @param CountryDirectoryServiceContract $service
     *
     * @return array
     */
    protected function country(CountryDirectoryServiceContract $service)
    {
        return $service->all();
    }

    /**
     * @param CryptoDirectoryServiceContract $service
     *
     * @return array
     */
    protected function crypto(CryptoDirectoryServiceContract $service)
    {
        return $service->all();
    }

    /**
     * @param FiatDirectoryServiceContract $service
     *
     * @return array
     */
    protected function fiat(FiatDirectoryServiceContract $service)
    {
        return $service->all();
    }

    /**
     * @param PaymentDirectoryServiceContract $service
     *
     * @return array
     */
    protected function payment(PaymentDirectoryServiceContract $service)
    {
        return $service->all();
    }

    /**
     * @param MarketDirectoryServiceContract $service
     *
     * @return array
     */
    protected function market(MarketDirectoryServiceContract $service)
    {
        return $service->all();
    }
}
```
