<?php

namespace p2pwatch\DirectoryDataService\Repositories;

use p2pwatch\DirectoryDataService\Contracts\Repositories\CountryCachedRepoContract;

class CountryCachedRepo extends AbstractCachedRepo implements CountryCachedRepoContract
{
    protected string $cacheKey = 'country';
}
