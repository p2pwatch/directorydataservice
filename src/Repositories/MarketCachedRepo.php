<?php

namespace p2pwatch\DirectoryDataService\Repositories;

use p2pwatch\DirectoryDataService\Contracts\Repositories\MarketCachedRepoContract;

class MarketCachedRepo extends AbstractCachedRepo implements MarketCachedRepoContract
{
    protected string $cacheKey = 'market';
}
