<?php

namespace p2pwatch\DirectoryDataService\Repositories;

use p2pwatch\DirectoryDataService\Contracts\Repositories\BankCachedRepoContract;

class BankCachedRepo extends AbstractCachedRepo implements BankCachedRepoContract
{
    protected string $cacheKey = 'bank';
}
