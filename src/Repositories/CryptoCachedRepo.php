<?php

namespace p2pwatch\DirectoryDataService\Repositories;

use p2pwatch\DirectoryDataService\Contracts\Repositories\CryptoCachedRepoContract;

class CryptoCachedRepo extends AbstractCachedRepo implements CryptoCachedRepoContract
{
    protected string $cacheKey = 'crypto';
}
