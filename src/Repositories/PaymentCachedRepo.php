<?php

namespace p2pwatch\DirectoryDataService\Repositories;

use p2pwatch\DirectoryDataService\Contracts\Repositories\PaymentCachedRepoContract;

class PaymentCachedRepo extends AbstractCachedRepo implements PaymentCachedRepoContract
{
    protected string $cacheKey = 'payment';
}
