<?php declare(strict_types=1);

namespace p2pwatch\DirectoryDataService\Repositories;

use Exception;
use Illuminate\Contracts\Cache\Repository;
use p2pwatch\DirectoryDataService\Contracts\Repositories\CachedRepoContract;
use p2pwatch\DirectoryDataService\Exceptions\NoKeyForDirectoryClass;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Class AbstractCachedRepo
 *
 * @package App\Repositories
 */
abstract class AbstractCachedRepo implements CachedRepoContract
{
    /**
     * Where data is stored
     */
    protected string $cacheKey;

    protected Repository $cache;

    /**
     * AbstractCachedRepo constructor.
     *
     * @param Repository $cache
     */
    public function __construct(Repository $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Get all current data
     *
     * @return array
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function all(): array
    {
        $this->validateKey();

        return $this->cache->get($this->getCacheKey()) ?? [];
    }

    /**
     * @return string
     */
    private function getCacheKey(): string
    {
        return config('p2pw-directorydataservice.directory_cache_key') . $this->cacheKey;
    }

    /**
     * @throws Exception
     */
    private function validateKey(): void
    {
        if (!$this->cacheKey) {
            throw new NoKeyForDirectoryClass('no key set for directory class');
        }
    }
}
