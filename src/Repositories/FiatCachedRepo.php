<?php

namespace p2pwatch\DirectoryDataService\Repositories;

use p2pwatch\DirectoryDataService\Contracts\Repositories\FiatCachedRepoContract;

class FiatCachedRepo extends AbstractCachedRepo implements FiatCachedRepoContract
{
    protected string $cacheKey = 'fiat';
}
