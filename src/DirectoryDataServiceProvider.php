<?php declare(strict_types=1);

namespace p2pwatch\DirectoryDataService;

use Illuminate\Support\ServiceProvider;
use p2pwatch\DirectoryDataService\Contracts\Repositories\BankCachedRepoContract;
use p2pwatch\DirectoryDataService\Contracts\Repositories\CountryCachedRepoContract;
use p2pwatch\DirectoryDataService\Contracts\Repositories\CryptoCachedRepoContract;
use p2pwatch\DirectoryDataService\Contracts\Repositories\FiatCachedRepoContract;
use p2pwatch\DirectoryDataService\Contracts\Repositories\MarketCachedRepoContract;
use p2pwatch\DirectoryDataService\Contracts\Repositories\PaymentCachedRepoContract;
use p2pwatch\DirectoryDataService\Contracts\Services\BankDirectoryServiceContract;
use p2pwatch\DirectoryDataService\Contracts\Services\CountryDirectoryServiceContract;
use p2pwatch\DirectoryDataService\Contracts\Services\CryptoDirectoryServiceContract;
use p2pwatch\DirectoryDataService\Contracts\Services\FiatDirectoryServiceContract;
use p2pwatch\DirectoryDataService\Contracts\Services\MarketDirectoryServiceContract;
use p2pwatch\DirectoryDataService\Contracts\Services\PaymentDirectoryServiceContract;
use p2pwatch\DirectoryDataService\Repositories\BankCachedRepo;
use p2pwatch\DirectoryDataService\Repositories\CountryCachedRepo;
use p2pwatch\DirectoryDataService\Repositories\CryptoCachedRepo;
use p2pwatch\DirectoryDataService\Repositories\FiatCachedRepo;
use p2pwatch\DirectoryDataService\Repositories\MarketCachedRepo;
use p2pwatch\DirectoryDataService\Repositories\PaymentCachedRepo;
use p2pwatch\DirectoryDataService\Services\BankDirectoryService;
use p2pwatch\DirectoryDataService\Services\CountryDirectoryService;
use p2pwatch\DirectoryDataService\Services\CryptoDirectoryService;
use p2pwatch\DirectoryDataService\Services\FiatDirectoryService;
use p2pwatch\DirectoryDataService\Services\MarketDirectoryService;
use p2pwatch\DirectoryDataService\Services\PaymentDirectoryService;

class DirectoryDataServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // services
        $this->app->bind(CountryDirectoryServiceContract::class, CountryDirectoryService::class);
        $this->app->bind(CryptoDirectoryServiceContract::class, CryptoDirectoryService::class);
        $this->app->bind(FiatDirectoryServiceContract::class, FiatDirectoryService::class);
        $this->app->bind(PaymentDirectoryServiceContract::class, PaymentDirectoryService::class);
        $this->app->bind(MarketDirectoryServiceContract::class, MarketDirectoryService::class);
        $this->app->bind(BankDirectoryServiceContract::class, BankDirectoryService::class);

        // repos
        $this->app->bind(CountryCachedRepoContract::class, CountryCachedRepo::class);
        $this->app->bind(CryptoCachedRepoContract::class, CryptoCachedRepo::class);
        $this->app->bind(FiatCachedRepoContract::class, FiatCachedRepo::class);
        $this->app->bind(PaymentCachedRepoContract::class, PaymentCachedRepo::class);
        $this->app->bind(MarketCachedRepoContract::class, MarketCachedRepo::class);
        $this->app->bind(BankCachedRepoContract::class, BankCachedRepo::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->publishes(
            [
                __DIR__ . '/p2pw-directorydataservice.php' => config_path('p2pw-directorydataservice.php'),
            ]
        );
    }
}
