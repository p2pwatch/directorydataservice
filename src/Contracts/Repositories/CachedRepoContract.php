<?php declare(strict_types=1);

namespace p2pwatch\DirectoryDataService\Contracts\Repositories;

/**
 * Interface CachedRepoContract
 *
 * @package App\Contracts\Backend\Repositories
 */
interface CachedRepoContract
{
    /**
     * Get all current data
     *
     * @return array
     */
    public function all(): array;
}
