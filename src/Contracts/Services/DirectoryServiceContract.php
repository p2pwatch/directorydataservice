<?php declare(strict_types=1);

namespace p2pwatch\DirectoryDataService\Contracts\Services;

interface DirectoryServiceContract
{
    /**
     * Get all current data
     *
     * @return array
     */
    public function all(): array;
}
