<?php declare(strict_types=1);

namespace p2pwatch\DirectoryDataService\Exceptions;

use Exception;

class NoKeyForDirectoryClass extends Exception
{

}
