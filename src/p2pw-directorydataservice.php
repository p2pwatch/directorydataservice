<?php declare(strict_types=1);

return [
    'directory_url'        => env('DIRECTORY_URL', ''),
    'directory_token'      => env('DIRECTORY_TOKEN', ''),
    'directory_cache_key'  => env('DIRECTORY_CACHE_KEY', 'directory:'),
];
