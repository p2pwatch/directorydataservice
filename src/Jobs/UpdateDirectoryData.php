<?php declare(strict_types=1);

namespace p2pwatch\DirectoryDataService\Jobs;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Response;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use JsonException;
use Psr\Http\Message\ResponseInterface;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Class UpdateDirectoryData
 *
 * Gets data from DataServer and saves to cache
 *
 * @package App\Jobs\Backend
 */
class UpdateDirectoryData
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Client $client;
    protected Repository $cache;
    private string $directoryUrl;
    private string $token;
    private string $cacheKey;

    public function __construct(Client $client, Repository $cache)
    {
        $this->client = $client;
        $this->directoryUrl = (string)config('p2pw-directorydataservice.directory_url');
        $this->token = (string)config('p2pw-directorydataservice.directory_token');
        $this->cache = $cache;
        $this->cacheKey = (string)config('p2pw-directorydataservice.directory_cache_key');
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws GuzzleException
     * @throws InvalidArgumentException
     * @throws JsonException
     */
    public function handle(): void
    {
        $response = $this->client->request('get', $this->directoryUrl, $this->getOptions());

        $this->parseResponse($response);
    }

    /**
     * @return array
     */
    private function getOptions(): array
    {
        return [
            'headers'     => [
                'Authorization' => md5($this->token),
            ],
            'timeout'     => 10,
            'http_errors' => false,
        ];
    }

    /**
     * @param ResponseInterface $response
     *
     * @throws JsonException
     * @throws InvalidArgumentException
     */
    protected function parseResponse(ResponseInterface $response): void
    {
        if ($response->getStatusCode() === Response::HTTP_OK) {
            $data = json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);

            collect($data)->each(
                function ($item, $key) {
                    $this->cache->set($this->cacheKey . $key, $item);
                }
            );
        } else {
            Log::critical('UpdateDirectoryData error with code ' . $response->getStatusCode());
        }
    }
}
