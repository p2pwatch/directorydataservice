<?php

namespace  p2pwatch\DirectoryDataService\Services;

use Exception;
use p2pwatch\DirectoryDataService\Contracts\Repositories\CachedRepoContract;
use p2pwatch\DirectoryDataService\Contracts\Services\DirectoryServiceContract;

/**
 * Class AbstractDirectoryService
 *
 * @package App\Services\Backend\Directory
 */
abstract class AbstractDirectoryService implements DirectoryServiceContract
{
    /**
     * @var CachedRepoContract
     */
    protected $repo;

    /**
     * Get all current data
     *
     * @return array
     * @throws Exception
     */
    public function all(): array
    {
        $this->validateRepo();

        return $this->repo->all();
    }

    /**
     * @throws Exception
     */
    private function validateRepo()
    {
        if (!$this->repo) {
            throw new Exception('no repo set for directory service class');
        }
    }
}
