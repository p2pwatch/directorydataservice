<?php

namespace p2pwatch\DirectoryDataService\Services;

use p2pwatch\DirectoryDataService\Contracts\Repositories\CountryCachedRepoContract;
use p2pwatch\DirectoryDataService\Contracts\Services\CountryDirectoryServiceContract;

class CountryDirectoryService extends AbstractDirectoryService implements CountryDirectoryServiceContract
{
    /**
     * CountryDirectoryService constructor.
     *
     * @param CountryCachedRepoContract $repo
     */
    public function __construct(CountryCachedRepoContract $repo)
    {
        $this->repo = $repo;
    }
}
