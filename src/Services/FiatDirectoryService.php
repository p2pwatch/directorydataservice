<?php

namespace p2pwatch\DirectoryDataService\Services;

use p2pwatch\DirectoryDataService\Contracts\Repositories\FiatCachedRepoContract;
use p2pwatch\DirectoryDataService\Contracts\Services\FiatDirectoryServiceContract;

class FiatDirectoryService extends AbstractDirectoryService implements FiatDirectoryServiceContract
{
    /**
     * FiatDirectoryService constructor.
     *
     * @param FiatCachedRepoContract $repo
     */
    public function __construct(FiatCachedRepoContract $repo)
    {
        $this->repo = $repo;
    }
}
