<?php

namespace p2pwatch\DirectoryDataService\Services;

use p2pwatch\DirectoryDataService\Contracts\Repositories\CryptoCachedRepoContract;
use p2pwatch\DirectoryDataService\Contracts\Services\CryptoDirectoryServiceContract;

class CryptoDirectoryService extends AbstractDirectoryService implements CryptoDirectoryServiceContract
{
    /**
     * CryptoDirectoryService constructor.
     *
     * @param CryptoCachedRepoContract $repo
     */
    public function __construct(CryptoCachedRepoContract $repo)
    {
        $this->repo = $repo;
    }
}
