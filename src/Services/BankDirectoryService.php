<?php

namespace p2pwatch\DirectoryDataService\Services;

use p2pwatch\DirectoryDataService\Contracts\Repositories\BankCachedRepoContract;
use p2pwatch\DirectoryDataService\Contracts\Services\BankDirectoryServiceContract;

class BankDirectoryService extends AbstractDirectoryService implements BankDirectoryServiceContract
{
    /**
     * BankDirectoryService constructor.
     *
     * @param BankCachedRepoContract $repo
     */
    public function __construct(BankCachedRepoContract $repo)
    {
        $this->repo = $repo;
    }
}
