<?php

namespace p2pwatch\DirectoryDataService\Services;

use p2pwatch\DirectoryDataService\Contracts\Repositories\MarketCachedRepoContract;
use p2pwatch\DirectoryDataService\Contracts\Services\MarketDirectoryServiceContract;

class MarketDirectoryService extends AbstractDirectoryService implements MarketDirectoryServiceContract
{
    /**
     * MarketDirectoryService constructor.
     *
     * @param MarketCachedRepoContract $repo
     */
    public function __construct(MarketCachedRepoContract $repo)
    {
        $this->repo = $repo;
    }
}
