<?php

namespace p2pwatch\DirectoryDataService\Services;

use p2pwatch\DirectoryDataService\Contracts\Repositories\PaymentCachedRepoContract;
use p2pwatch\DirectoryDataService\Contracts\Services\PaymentDirectoryServiceContract;

class PaymentDirectoryService extends AbstractDirectoryService implements PaymentDirectoryServiceContract
{
    /**
     * PaymentDirectoryService constructor.
     *
     * @param PaymentCachedRepoContract $repo
     */
    public function __construct(PaymentCachedRepoContract $repo)
    {
        $this->repo = $repo;
    }
}
